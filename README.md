# Firebase 
CRUD practice with Realtime Database 
## INSTRUCTION
1. Go to firebase console
2. Register or Login with your gmail 
3. Create a project
4. Create an app
4. Get your private key(for python) : Project Setting/ Service accounts
5. Create a database : Build / Realtime database

## Setup 
1. Clone this repository
2. Download and keep the private key(as in Instruction 4) in same directory
3. Make necessary replacement in the code

**Enjoy !**